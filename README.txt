Rendering 3D Objects Using Octrees
Joshua Ertl

Utilizes algorithm proposed by:
"An Efficient Parametric Algorithm for Octree Traversal"
J. Revelles, C. Urena, M. Lastra


Program takes two optional arguments, without any the defaults will be:
max depth = 16		file name = "model.ptxm"

1st argument is a number that changes max depth for storing data
2nd argument is a string with no spaces that specifies a model file


Controls:
W A S D             = control horizontal movement based on world axis, not the camera
R F                 = increase / decrease height
Q E                 = control rotation
Z                   = toggles the black origin platform from rendering
Up / Down arrow     = increase / decrease render depth
Left arrow          = set the render depth to 1
Right arrow         = set the render depth to max depth


PTXM Files:
Theses are not true .ptx format files, they have been modified to exclude the header information
and keep most of the point data, excluding the intensity value.
Format for ".ptxm" is as follows:

(int number of points)
(float min X) (float min Y) (float min Z) (float max X) (float max Y) (float max Z)
(float point X) (float point Y) (float point Z) (int red using 255) (int green using 255) (int blue using 255)
...

The "min" and "max" values represent the boundary of the object, they can either be the actual min and max of points
or can be a slight excess, but will always be the bounds of the object.
There are "number of points" number of "(float point X) ..." lines that represent the raw point cloud data


Provided PTXM Files:
temp.ptxm   = 5 points  A demo file with the simplest possible structure
bunny.ptxm  = 34834 points  The Stanford bunny from .off format converted and assigned color
dog.ptxm    = 195586 points  Provided file from .off format converted and assigned color
pumpA.ptxm  = 155201 points  Converted real .ptx format file  http://www.libe57.org/data.html